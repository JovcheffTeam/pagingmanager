# Paginator

[![CI Status](http://img.shields.io/travis/Dyanko Yovchev/Paginator.svg?style=flat)](https://travis-ci.org/Dyanko Yovchev/Paginator)
[![Version](https://img.shields.io/cocoapods/v/Paginator.svg?style=flat)](http://cocoapods.org/pods/Paginator)
[![License](https://img.shields.io/cocoapods/l/Paginator.svg?style=flat)](http://cocoapods.org/pods/Paginator)
[![Platform](https://img.shields.io/cocoapods/p/Paginator.svg?style=flat)](http://cocoapods.org/pods/Paginator)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Paginator is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Paginator'
```

## Author

Dyanko Yovchev, dyanko.yovchev@upnetix.com

## License

Paginator is available under the MIT license. See the LICENSE file for more info.
